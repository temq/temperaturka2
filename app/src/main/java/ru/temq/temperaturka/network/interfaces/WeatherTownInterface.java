package ru.temq.temperaturka.network.interfaces;


import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import ru.temq.temperaturka.model.Temperature;
import rx.Observable;

/**
 * Created by temq on 19.05.15.
 */
public interface WeatherTownInterface {

    @GET("/weather")
    Observable<Temperature> loadWeatherForTownById(@Query("id") long id);
}
