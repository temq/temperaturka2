package ru.temq.temperaturka;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by temq on 24.06.15.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class UtilsTest {

    @Test
    public void utils(){
        String str = Utils.stringConverter(5);

        Assert.assertEquals(str, "5");
    }


}
