package ru.temq.temperaturka.network;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import ru.temq.temperaturka.GlobalConstants;
import ru.temq.temperaturka.model.Temperature;
import ru.temq.temperaturka.network.interfaces.WeatherTownInterface;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by temq on 08.07.15.
 */
public class WeatherLoader {

    private final WeatherTownInterface townInterface;

    public WeatherLoader() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(GlobalConstants.API_URI)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        townInterface = adapter.create(WeatherTownInterface.class);
    }

    public void loadWeatherForTown(long townId) {
        townInterface.loadWeatherForTownById(townId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(60, TimeUnit.SECONDS)
                .retry(2)
                .onErrorReturn(new Func1<Throwable, Temperature>() {
                    @Override
                    public Temperature call(Throwable throwable) {
                        Log.i(GlobalConstants.APP_TAG, throwable.getMessage());
                        return null;
                    }
                })
                .doOnNext(new Action1<Temperature>() {
                    @Override
                    public void call(Temperature temperature) {

                    }
                }).subscribe(new Action1<Temperature>() {
                    @Override
                    public void call(Temperature temperature) {
                        if (temperature != null) {
                            Log.d(GlobalConstants.APP_TAG, temperature.toString());
                        }
                    }
                });
    }

}
