package ru.temq.temperaturka.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by temq on 08.07.15.
 */
public class Coordinates {

    @SerializedName("lon")
    private float longitude;

    @SerializedName("lat")
    private float lattitude;

    public Coordinates(float longitude, float lattitude) {
        this.longitude = longitude;
        this.lattitude = lattitude;
    }

    public Coordinates(){}

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLattitude() {
        return lattitude;
    }

    public void setLattitude(float lattitude) {
        this.lattitude = lattitude;
    }
}
