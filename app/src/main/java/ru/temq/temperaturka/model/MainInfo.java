package ru.temq.temperaturka.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by temq on 08.07.15.
 */
public class MainInfo {

    private float temperature;
    private int humidity;
    private int pressure;
    @SerializedName("temp_max")
    private float tempMax;
    @SerializedName("temp_min")
    private float tempMin;

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public float getTempMax() {
        return tempMax;
    }

    public void setTempMax(float tempMax) {
        this.tempMax = tempMax;
    }

    public float getTempMin() {
        return tempMin;
    }

    public void setTempMin(float tempMin) {
        this.tempMin = tempMin;
    }
}
