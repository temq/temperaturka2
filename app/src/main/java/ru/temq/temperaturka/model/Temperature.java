package ru.temq.temperaturka.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by temq on 08.07.15.
 */
public class Temperature {

    @SerializedName("coord")
    private Coordinates coordinates;
    @SerializedName("main")
    private MainInfo mainInfo;
    private List<Weather> weathers;
    private Wind wind;
    @SerializedName("sys")
    private Info info;
    @SerializedName("dt")
    private long timestamp;
    private long id;
    private String name;
    private long cod;

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public MainInfo getMainInfo() {
        return mainInfo;
    }

    public void setMainInfo(MainInfo mainInfo) {
        this.mainInfo = mainInfo;
    }

    public List<Weather> getWeathers() {
        return weathers;
    }

    public void setWeathers(List<Weather> weathers) {
        this.weathers = weathers;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCod() {
        return cod;
    }

    public void setCod(long cod) {
        this.cod = cod;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
