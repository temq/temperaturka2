package ru.temq.temperaturka.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by temq on 08.07.15.
 */
public class Wind {

    private float speed;
    @SerializedName("deg")
    private float degrease;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDegrease() {
        return degrease;
    }

    public void setDegrease(float degrease) {
        this.degrease = degrease;
    }
}
