package ru.temq.temperaturka;

/**
 * Created by temq on 08.07.15.
 */
public class GlobalConstants  {

    public static final String API_URI = "http://api.openweathermap.org/data/2.5/";
    public static final String APP_TAG = "Temperaturka";

}
